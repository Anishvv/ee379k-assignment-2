package question5a;

public interface Protocol {
	public void enterMale();

	public void leaveMale();

	public void enterFemale();

	public void leaveFemale();
}
