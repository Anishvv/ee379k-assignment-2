package question5a;

import question5a.BathroomLockProtocol;

public class Q5AMain {
	
	static BathroomLockProtocol blp;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		blp = new BathroomLockProtocol();
		Thread male = new Thread(m1);
		Thread male1 = new Thread(m1);
		Thread male2 = new Thread(m2);
		Thread male3 = new Thread(m1);
		Thread male4 = new Thread(m2);
		Thread male5 = new Thread(m2);
		Thread male6 = new Thread(m1);
		Thread female = new Thread(f1);
		Thread female1 = new Thread(f1);
		Thread female2 = new Thread(f2);
		female.start();
		male.start();
		male1.start();
		male3.start();
		female2.start();
		male4.start();
		male5.start();
		male6.start();
		female1.start();
		male2.start();
	}
	
	static Runnable m1 = new Runnable(){
		@Override
		public void run() {
			// TODO Auto-generated method stub
			blp.enterMale();
			blp.leaveMale();
		}
	};
	static Runnable m2 = new Runnable(){
		@Override
		public void run() {
			// TODO Auto-generated method stub
			blp.enterMale();
			for(Integer i = 0; i < i.MAX_VALUE; i++){
				i = i;
			}
			blp.leaveMale();
		}
	};
	static Runnable f1 = new Runnable(){
		@Override
		public void run() {
			// TODO Auto-generated method stub
			blp.enterFemale();
			blp.leaveFemale();
		}
	};
	static Runnable f2 = new Runnable(){
		@Override
		public void run() {
			// TODO Auto-generated method stub
			blp.enterFemale();
			for(Integer i = 0; i < i.MAX_VALUE; i++){
				i = i;
			}
			blp.leaveFemale();
		}
	};
}
