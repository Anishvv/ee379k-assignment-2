package question5a;
// TODO
// Use locks and condition variables to implement the bathroom protocol
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BathroomLockProtocol implements Protocol {
	// declare the lock and conditions here
	  int males;
	  int females;
	  int victor = -1;
	  Lock lock;
	  Condition condition;
	
	public BathroomLockProtocol(){
		males = 0;
		females = 0;
		lock = new ReentrantLock();
		condition = lock.newCondition();
	}
	public void enterMale() {
		lock.lock();
		//System.out.println("Male trying to enter");
	    try {
	        while (females > 0 || (victor == 0)) {
	        	//System.out.println("Male waiting");
	        	if(victor == -1){
	        		victor = 1;
	        	}
		        try {
		        	condition.await();
			    } catch (InterruptedException e) {}
		    }
	        if(victor == 1){
	        	victor = -1;
	        }
	    	males++;
	    } finally {
	    	//System.out.println("Male entered");
	        lock.unlock();
	    }
	}

	public void leaveMale() {
		lock.lock();
	    try {
	        males--;
	        //System.out.println("Male left");
	        if (males == 0){
	        	//victor = -1;
	        	//System.out.println("All males out");
	        	condition.signalAll();
	        }
	    } finally {
	        lock.unlock();
	    }
	}

	public void enterFemale() {
		lock.lock();
		//System.out.println("Female trying to enter");
	    try {
	        while (males > 0 || (victor == 1)) {
	        	System.out.println("Female waiting");
	        	if(victor == -1){
	        		victor = 0;
	        	}
	        	try {
		            condition.await();
		        } catch (InterruptedException e) {}
	        }
	        if(victor == 0){
	        	victor = -1;
	        }
	        females++;
	    } finally {
	    	//System.out.println("Female entered");
	    	lock.unlock();
	    }
	}

	public void leaveFemale() {
		lock.lock();
	    try {
	        females--;
	        //System.out.println("Female left");
	        if (females == 0){
	        	//victor = -1;
	        	//System.out.println("All females out");
	        	condition.signalAll();
	        }
	    } finally {
	    	lock.unlock();
	    }
	}
}
