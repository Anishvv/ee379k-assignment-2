package question5b;

public interface Protocol {
	public void enterMale();

	public void leaveMale();
	public void enterFemale();

	public void leaveFemale();
}
