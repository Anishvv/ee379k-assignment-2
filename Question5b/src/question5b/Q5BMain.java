package question5b;

import question5b.BathroomSynProtocol;

public class Q5BMain {
	
	static BathroomSynProtocol bsp;

	public static void main(String[] args) {
		bsp = new BathroomSynProtocol();
		Thread male = new Thread(m1);
		Thread male1 = new Thread(m1);
		Thread male2 = new Thread(m2);
		Thread male3 = new Thread(m1);
		Thread male4 = new Thread(m2);
		Thread male5 = new Thread(m2);
		Thread male6 = new Thread(m1);
		Thread female = new Thread(f1);
		Thread female1 = new Thread(f1);
		Thread female2 = new Thread(f2);
		female.start();
		male.start();
		male1.start();
		male3.start();
		female2.start();
		male4.start();
		male5.start();
		male6.start();
		female1.start();
		male2.start();
	}
	
	static Runnable m1 = new Runnable(){
		@Override
		public void run() {
			// TODO Auto-generated method stub
			bsp.enterMale();
			bsp.leaveMale();
		}
	};
	static Runnable m2 = new Runnable(){
		@Override
		public void run() {
			// TODO Auto-generated method stub
			bsp.enterMale();
			for(Integer i = 0; i < i.MAX_VALUE; i++){
				i = i;
			}
			bsp.leaveMale();
		}
	};
	static Runnable f1 = new Runnable(){
		@Override
		public void run() {
			// TODO Auto-generated method stub
			bsp.enterFemale();
			bsp.leaveFemale();
		}
	};
	static Runnable f2 = new Runnable(){
		@Override
		public void run() {
			// TODO Auto-generated method stub
			bsp.enterFemale();
			for(Integer i = 0; i < i.MAX_VALUE; i++){
				i = i;
			}
			bsp.leaveFemale();
		}
	};
	
}
